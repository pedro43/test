﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Prueba.Models;

namespace Prueba
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            Singleton Instancia = Singleton.GetInstance();

            // Inicialization Bars
            Instancia.Bars.Add(new Bar()
            {
                IdBar = 1,
                AddressBar = "Melo 2463",
                NameBar = "Charrua",
                Students = new List<Student>()
            });

            Instancia.Bars.Add(new Bar()
            {
                IdBar = 2,
                AddressBar = "18 de Julio 1542",
                NameBar = "Bremen",
                Students = new List<Student>()
            });

            Instancia.Bars.Add(new Bar()
            {
                IdBar = 3,
                AddressBar = "San Martin 4242",
                NameBar = "Clash",
                Students = new List<Student>()
            });

            Instancia.Bars.Add(new Bar()
            {
                IdBar = 4,
                AddressBar = "Marmaraja 2489",
                NameBar = "Phoenix",
                Students = new List<Student>()
            });
            Instancia.Bars.Add(new Bar()
            {
                IdBar = 5,
                AddressBar = "Martin Garcia 7458",
                NameBar = "Ilegal",
                Students = new List<Student>()
            });


        }
    }
}
