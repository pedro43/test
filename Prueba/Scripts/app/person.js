﻿$(document).on("ready", function () {
    GetAll();
})


function GetAll() {
    var item = "";
    $('#tblList tbody').html('');
    $.getJSON('/api/People/GetPeople', function (data) {
        $.each(data, function (key, value) {
            item += "<tr><td> " + value.Name + "</td><td>" + value.LastName + "</td></tr>";
        });
        $('#tblList tbody').append(item);
    });
};

function GetPersonById(idPerson) {
    var url = '/api/People/GetPerson/' + idPerson;
    $.getJSON(url)
        .done(function (data) {
            $('#txtName').val(data.Name);
            $('#txtLastName').val(data.LastName);
           
        })
        .fail(function (erro) {
            ClearForm();
        });
};

$('#btnSearch').on('click', function () {
    GetPersonById($('#txtIdSearch').val());
})

$('#btnSearchName').on('click', function () {
    GetPersonByName($('#txtNameSearch').val());
})

function GetPersonByName(NamePerson) {
    var item = "";
    $('#tblList tbody').html('');
    var url = '/api/People/GetByName/'+ NamePerson;
    $.getJSON(url, function(data)
    {
        $.each(data, function (key, value) {
            item += "<tr><td> " + value.Name + "</td><td>" + value.LastName + "</td></tr>";
        });
        $('#tblList tbody').append(item);
    });
        
};

function DeletePersonById(idPerson) {
    var url = '/api/People/GetPerson/' + idPerson;
    $.ajax({
        url: url,
        type: 'DELETE',
        contentType: "application/json;chartset=utf-8",
        statusCode: {
            200: function () {
                GetAll();
                ClearForm();
                alert('Person with id: ' + idPerson + ' was deleted');
            },
            404: function () {
                alert('Person with id: ' + idPerson + ' was not found');
            }
        }
    });
}


$('#btnDelete').on('click', function () {
    DeletePersonById($('#txtIdSearch').val());
})

function UpdatePerson(idPerson, person) {
    var url = '/api/People/PutPerson/' + idPerson;
    $.ajax({
        url: url,
        type: 'PUT',
        data: person,
        contentType: "application/json;chartset=utf-8",
        statusCode: {
            200: function () {
                GetAll();
                ClearForm();
                alert('Person with id: ' + idPerson + ' was updated');
            },
            404: function () {
                ClearForm();
                alert('Person with id: ' + idPerson + ' was not found');
            },
            400: function () {
                ClearForm();
                alert('Error');
            }
        }
    });
}

$('#btnUpdate').on('click', function () {
    var person = new Object();
    person.id = $('#txtIdSearch').val();
    person.name = $('#txtName').val();
    person.lastname = $('#txtLastName').val();
    person.twitter = $('#txtTwitter').val();
    UpdatePerson(person.id, JSON.stringify(person));
})


function CreatePerson(person) {
    var url = '/api/People/PostPerson';
    $.ajax({
        url: url,
        type: 'POST',
        data: person,
        contentType: "application/json;chartset=utf-8",
        statusCode: {
            201: function () {
                GetAll();
                ClearForm();
                alert('Person with id: ' + idPerson + ' was updated');
            },
            400: function () {
                ClearForm();
                alert('Error');
            }
        }
    });
}



$('#btnCreate').on('click', function () {
    var person = new Object();
    person.name = $('#txtName').val();
    person.lastname = $('#txtLastName').val();
   
    CreatePerson(JSON.stringify(person));
})

