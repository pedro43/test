﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Prueba.Models
{
    public class Bar
    {
        public int IdBar { get; set; }
        public string NameBar { get; set; }
        public string AddressBar { get; set; }
        public List<Student> Students { get; set; }
        public List<Bar> Bars { get; set; }
    }
}