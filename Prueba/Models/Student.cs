﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Prueba.Models
{
    public class Student
    {
        public int id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public List<Bar> Bars { get; set; }
        // Comment Diego

     public Student()
        {
            Bars = new List<Bar>();
        }

        public static Student GetStudent (int id)
        {

            return Singleton.GetInstance().Students.Where(s => s.id == id).FirstOrDefault();
        }

        public bool VisitsBar(int idBar)
        {
            Singleton Instancia = Singleton.GetInstance();
            
            Bar BarAux = Instancia.Bars.Where(b => b.IdBar == idBar).FirstOrDefault();

            if(BarAux != null)
            {
                BarAux.Students.Add(this);
                this.Bars.Add(BarAux);
                return true;
            }
            return false;
        }

        public void Initialize()
        {
            Name = string.Empty;
            Age = 0;
            id = 0;
        }

    }
}