﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Prueba.Models
{
    public class Singleton
    {
        private static Singleton Instance = new Singleton();
        public List<Student> Students = new List<Student>();
        public List<Bar> Bars = new List<Bar>();


        private Singleton() { }

        public static Singleton GetInstance()
        {
            
            if(Instance == null)
            {
                Instance = new Singleton();
            }
            return Instance;
        }
    }
}