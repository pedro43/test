﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Prueba.Models;
using Prueba.Controllers;
using System.Linq;
using System.Collections.Generic;

namespace Prueba.Tests.Controllers
{
    [TestClass]
    public class PersonTest
    {
        
        static PeopleController PC = new PeopleController();
        PersonContext context = PC.DB;

        [TestMethod]
        public void AddTest()
        {
            Person Aux = new Person { Name = "Diego", LastName = "Rodriguez" };


            context.People.Add(Aux);
            context.SaveChanges();

            int Ultimo = context.People.Max(x => x.id);
            Person Resultado = context.People.Find(Ultimo);

            Assert.AreEqual(Resultado.id, Aux.id);
            Assert.AreEqual<string>(Resultado.Name, Aux.Name);
            Assert.AreEqual<string>(Resultado.LastName, Aux.LastName);
           
        }

        [TestMethod]
        public void EditTest()
        {
            Person Aux = new Person { Name = "Diego", LastName = "Rodriguez" };
            context.People.Add(Aux);
            int Ultimo = context.People.Max(x => x.id);
            Aux = context.People.Find(Ultimo);
            Aux.LastName = "Skupnjak";
            PC.PutPerson(Ultimo, Aux);
            context.SaveChanges();

            Person Resultado = context.People.Find(Ultimo);

            Assert.AreEqual<int>(Resultado.id, Aux.id);
            Assert.AreEqual<string>(Resultado.Name, Aux.Name);
            Assert.AreEqual<string>(Resultado.LastName, Aux.LastName);
           

        }

        [TestMethod][ExpectedException(typeof(NullReferenceException))]
        public void EditNullTest()
        {
            Person aux = context.People.Find(999);
            PC.PutPerson(999, aux);
        }

    
        [TestMethod]
        public void DeleteTest()
        {
            Person Aux = new Person { Name = "Diego", LastName = "Rodriguez" };
           

            context.People.Add(Aux);
            context.SaveChanges();

            int Ultimo = context.People.Max(x => x.id);

            Person Ingresado = context.People.Find(Ultimo);

            if(Ingresado != null)
            {
                PC.DeletePerson(Ultimo);
                
            }
            

            Person Resultado = context.People.Find(Ultimo);

            Assert.AreEqual<Object>(Resultado, null);

          
        }

        [TestMethod]
        public void GetAllTest()
        {
            Person Aux1 = new Person { Name = "Diego1", LastName = "Rodriguez" };
            Person Aux2 = new Person { Name = "Diego2", LastName = "Rodriguez" };
            Person Aux3 = new Person { Name = "Diego3", LastName = "Rodriguez" };
            Person Aux4 = new Person { Name = "Diego4", LastName = "Rodriguez" };
            Person Aux5 = new Person { Name = "Diego5", LastName = "Rodriguez" };

            context.People.Add(Aux1);
            context.People.Add(Aux2);
            context.People.Add(Aux3);
            context.People.Add(Aux4);
            context.People.Add(Aux5);
            context.SaveChanges();

            int Contador1 = context.People.Count();
            int Contador2 = PC.GetPeople().Count();

            Assert.AreEqual<int>(Contador1, Contador2);
        }

        [TestMethod]
        public void GetByNameTest()
        {

            context.People.Add(new Person { Name = "Roberto", LastName = "Ramirez" });
            context.People.Add(new Person { Name = "Fernando", LastName = "Espantoso" });
            context.People.Add(new Person { Name = "Fernanda", LastName = "Pardiñas" });
            context.SaveChanges();
            
            string Name = "Fe";
            List<Person> List1 = PC.GetByName("Fe").ToList() ;
            List<Person> List2 = context.People.Where(c => c.Name.Contains(Name)).ToList();
            if (List1.Count == List2.Count)
            {
                for (int i = 0; i < List1.Count; i++)
                {
                    Assert.AreEqual<int>(List1[i].id, List2[i].id);
                    Assert.AreEqual<string>(List1[i].Name, List2[i].Name);
                    Assert.AreEqual<string>(List1[i].LastName, List2[i].LastName);
                }
            }
            
        }

   
    }
}
